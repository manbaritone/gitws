import pandas as pd
from apyori import apriori
from sklearn import model_selection
import argparse

def load_data(filename):
    data = pd.read_csv(filename)
    return data
data = load_data('store_data.csv')

def preprocess(data):   
    item_list = data.unstack().dropna().unique()
    print(item_list)
    print("# items:", len(item_list))

    train, test = model_selection.train_test_split(data, test_size=.10)
    print("train:", train)
    print("test:", test)

    train = train.T.apply(lambda x: x.dropna().tolist()).tolist()
    print("transforms")
    print(train)
    for i in train[:10]:
        print(i)
        
    return train, test


def model(train_data, min_support=0.0045, min_confidence=0.2, min_lift=3, min_length=2):
    results = list(apriori(train_data, min_support=min_support, min_confidence=min_confidence, min_lift=min_lift, min_length=min_length))
    print("results")
    print(results)
    return results

def visualize(results):
    for rule in results:
        pair = rule[0]
        components = [i for i in pair]
        print(pair)
        print('Rule:', components[0], '->', components[1])
        print('Support', rule[1])
        print('Confidence', rule[2][0][2])
        print('Lift', rule[2][0][3])
        print("===========================")
        
def argument():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', type=str, help="csv file as dataset")
    parser.add_argument('--min_length', '-m', type=int, default=3, help="minimum number of product")
    return parser

if __name__ == "__main__":
    parser = argument()
    args = parser.parse_args()
    data = load_data('store_data.csv')
    data_2 = load_data('store2_data.csv')
    train, test = preprocess(data)
    result = model(train, min_length=args.min_length)
    visualize(result)